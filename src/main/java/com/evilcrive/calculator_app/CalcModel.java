package com.evilcrive.calculator_app;

public class CalcModel {
    CalcController controller;

    Double firstNumber, secondNumber, result;
    Integer digits;
    Boolean doneFirstNumber, firstTimeSecondNumber, doneSecondNumber, hasComma;
    String operation;

    public CalcModel(CalcController obj) {
        firstNumber = 0d;
        secondNumber = 0d;
        result = 0d;

        digits = 0;

        doneFirstNumber = false;
        firstTimeSecondNumber = false;
        doneSecondNumber = false;
        hasComma = false;

        operation = "";

        controller = obj;
    }

    public void clickedButton(String event) {
        if(event.length() == 1 && Character.isDigit(event.charAt(0))){
            receivedDigit(Integer.parseInt(event));
        }else   receivedOperation(event);
    }

    // 0 for only main textField, 1 for both
    public void resetText(Integer arg) {
        controller.resetText(arg);
    }

    public void writeDigitMain(Integer arg, Integer arg1){
        controller.writeDigitMain(arg, arg1);
    }

    public void overwriteNumber(String toWriteMain, String toWriteSide, Integer arg){
        controller.overwriteNumber(toWriteMain, toWriteSide, arg);
    }
    public void changeNegativeSign(Integer arg){
        controller.changeNegativeSign(arg);
    }
    public void removeLastChar(){
        controller.removeLastChar();
    }

    public void receivedDigit(Integer digit) {
        if (doneFirstNumber == false) {
            if (hasComma == false) {
                firstNumber = firstNumber * 10 + Double.valueOf(digit);
            }else{
                Double numberToAdd = Double.valueOf(digit) / 10;
                for(int i = 0;i < digits; i++){
                    numberToAdd /= 10;
                }
                firstNumber = firstNumber + numberToAdd;
                digits++;
            }
            writeDigitMain(digit, 0);
        }else if (doneSecondNumber == false){
            if (hasComma == false) {
                secondNumber = secondNumber * 10 + Double.valueOf(digit);
            }else{
                Double numberToAdd = Double.valueOf(digit) / 10;
                for(int i = 0;i < digits; i++){
                    numberToAdd /= 10;
                }
                firstNumber = firstNumber + numberToAdd;
                digits++;
            }
            if(firstTimeSecondNumber){
                writeDigitMain(digit, 1);
                firstTimeSecondNumber = false;
            }else   writeDigitMain(digit, 0);
        }else{
            resetText(1);
            firstNumber = Double.valueOf(digit);
            writeDigitMain(digit, 0);

            doneFirstNumber = false;            
            doneSecondNumber = false;
            firstTimeSecondNumber = false;
            hasComma = false;
            operation = "";
            secondNumber = 0d;
            result = 0d;
        }

    }
    public void receivedOperation(String event){
        switch(event) {
            case "10^x":
                if(doneFirstNumber == false){
                    String toWriteSide ="10^(" + Double.toString(firstNumber) + ")";
                    firstNumber = Math.pow(10, firstNumber);
                    String toWriteMain = Double.toString(firstNumber);
                    overwriteNumber(toWriteMain, toWriteSide, 0);
                }else if (doneSecondNumber == false){
                    String toWriteSide = "10^(" + Double.toString(secondNumber) + ")";
                    secondNumber = Math.pow(10, secondNumber);
                    String toWriteMain = Double.toString(secondNumber);
                    overwriteNumber(toWriteMain, toWriteSide, 1);
                }else{
                    firstNumber = Math.pow(10, result);
                    secondNumber = 0d;
                    String toWriteSide ="10^(" + Double.toString(result) + ")";
                    String toWriteMain = Double.toString(firstNumber);
                    doneFirstNumber = false;
                    doneSecondNumber = false;
                    hasComma = false;
                    result = 0d;
                    digits = 0; 
                    overwriteNumber(toWriteMain, toWriteSide, 0);
                }
                break;
            case "CE":
                if (doneFirstNumber == true &&
                     doneSecondNumber == false){
                    secondNumber = 0d;
                    resetText(3);
                }else{
                    firstNumber = 0d;
                    secondNumber = 0d;
                    resetText(3);
                }
                break;
            case "C":
                resetText(2);
                firstNumber = 0d;
                doneFirstNumber = false;            
                doneSecondNumber = false;
                firstTimeSecondNumber = false;
                hasComma = false;
                operation = "";
                secondNumber = 0d;
                result = 0d;
                break;
            case "del":
                if(doneFirstNumber == false && firstNumber != 0d){
                    String s_fNumber = Double.toString(firstNumber);
                    if (hasComma)   s_fNumber = s_fNumber.substring(0, s_fNumber.length()-1);
                    else    s_fNumber = s_fNumber.substring(0, s_fNumber.length()-3);
                    firstNumber = Double.parseDouble(s_fNumber);
                    System.out.println(s_fNumber);

                    
                    removeLastChar();
                }else if (doneSecondNumber == false && secondNumber != 0d){
                    String s_sNumber = Double.toString(secondNumber);
                    s_sNumber = s_sNumber.substring(0, s_sNumber.length()-1);
                    secondNumber = Double.parseDouble(s_sNumber);
                    removeLastChar();
                }else {
                    resetText(2);
                    firstNumber = 0d;
                    doneFirstNumber = false;            
                    doneSecondNumber = false;
                    firstTimeSecondNumber = false;
                    hasComma = false;
                    operation = "";
                    secondNumber = 0d;
                    result = 0d;
                }
                break;
            case "^":
                operation = "^";
                if(doneFirstNumber == false){
                    String toWriteSide = Double.toString(firstNumber)+" ^ "; 
                    String toWriteMain = Double.toString(firstNumber);
                    doneFirstNumber = true;
                    firstTimeSecondNumber = true;
                    digits = 0; 
                    hasComma=false;
                    overwriteNumber(toWriteMain, toWriteSide, 0);
                }else if(doneSecondNumber == false){
                    String toWriteSide = Double.toString(secondNumber);
                    result = Math.pow(firstNumber, secondNumber);
                    String toWriteMain = Double.toString(result);
                    secondNumber = 0d;
                    firstNumber = 0d;
                    doneSecondNumber = true;
                    digits = 0; 
                    hasComma=false;
                    overwriteNumber(toWriteMain, toWriteSide, 1);
                }else{
                    firstNumber = result;
                    result = 0d;
                    doneFirstNumber = true;
                    doneSecondNumber = false;
                    firstTimeSecondNumber = true;
                    digits = 0; 
                    hasComma=false;
                    String toWriteSide = Double.toString(firstNumber)+" ^ "; 
                    String toWriteMain = Double.toString(firstNumber);
                    overwriteNumber(toWriteMain, toWriteSide, 0);
                }  
                break;
            case "square":
                if(doneFirstNumber == false){
                    String toWriteSide ="sqr(" + Double.toString(firstNumber) + ")";
                    firstNumber = Math.pow(firstNumber, 2);
                    String toWriteMain = Double.toString(firstNumber);
                    overwriteNumber(toWriteMain, toWriteSide, 0);
                }else if (doneSecondNumber == false){
                    String toWriteSide = "sqr(" + Double.toString(secondNumber) + ")";
                    secondNumber = Math.pow(secondNumber, 2);
                    String toWriteMain = Double.toString(secondNumber);
                    overwriteNumber(toWriteMain, toWriteSide, 1);
                }else{
                    firstNumber = Math.pow(result, 2);
                    secondNumber = 0d;
                    String toWriteSide ="sqr(" + Double.toString(result) + ")";
                    String toWriteMain = Double.toString(firstNumber);
                    doneFirstNumber = false;
                    doneSecondNumber = false;
                    hasComma = false;
                    result = 0d;
                    digits = 0; 
                    overwriteNumber(toWriteMain, toWriteSide, 0);
                }
                break;
            case "sqrt":
                if(doneFirstNumber == false){
                    String toWriteSide ="sqrt(" + Double.toString(firstNumber) + ")";
                    firstNumber = Math.sqrt(firstNumber);
                    String toWriteMain = Double.toString(firstNumber);
                    overwriteNumber(toWriteMain, toWriteSide, 0);
                }else if (doneSecondNumber == false){
                    String toWriteSide = "sqrt(" + Double.toString(secondNumber) + ")";
                    secondNumber = Math.sqrt(secondNumber);
                    String toWriteMain = Double.toString(secondNumber);
                    overwriteNumber(toWriteMain, toWriteSide, 1);
                }else{
                    firstNumber = Math.sqrt(result);
                    secondNumber = 0d;
                    String toWriteSide ="sqrt(" + Double.toString(result) + ")";
                    String toWriteMain = Double.toString(firstNumber);
                    doneFirstNumber = false;
                    doneSecondNumber = false;
                    hasComma = false;
                    result = 0d;
                    digits = 0; 
                    overwriteNumber(toWriteMain, toWriteSide, 0);
                }
                break;
            case "/":
                operation = "/";
                if(doneFirstNumber == false){
                    String toWriteSide = Double.toString(firstNumber)+" / "; 
                    String toWriteMain = Double.toString(firstNumber);
                    doneFirstNumber = true;
                    firstTimeSecondNumber = true;
                    digits = 0; 
                    hasComma=false;
                    overwriteNumber(toWriteMain, toWriteSide, 0);
                }else if(doneSecondNumber == false){
                    String toWriteSide = Double.toString(secondNumber);
                    result = firstNumber / secondNumber;
                    String toWriteMain = Double.toString(result);
                    secondNumber = 0d;
                    firstNumber = 0d;
                    doneSecondNumber = true;
                    digits = 0; 
                    hasComma=false;
                    overwriteNumber(toWriteMain, toWriteSide, 1);
                }else{
                    firstNumber = result;
                    result = 0d;
                    doneFirstNumber = true;
                    doneSecondNumber = false;
                    firstTimeSecondNumber = true;
                    digits = 0; 
                    hasComma=false;
                    String toWriteSide = Double.toString(firstNumber)+" / "; 
                    String toWriteMain = Double.toString(firstNumber);
                    overwriteNumber(toWriteMain, toWriteSide, 0);
                }   
                break;
            case "x":
                operation = "x";
                if(doneFirstNumber == false){
                    String toWriteSide = Double.toString(firstNumber)+" * "; 
                    String toWriteMain = Double.toString(firstNumber);
                    doneFirstNumber = true;
                    firstTimeSecondNumber = true;
                    digits = 0; 
                    hasComma=false;
                    overwriteNumber(toWriteMain, toWriteSide, 0);
                }else if(doneSecondNumber == false){
                    String toWriteSide = Double.toString(secondNumber);
                    result = firstNumber * secondNumber;
                    String toWriteMain = Double.toString(result);
                    secondNumber = 0d;
                    firstNumber = 0d;
                    doneSecondNumber = true;
                    digits = 0; 
                    hasComma=false;
                    overwriteNumber(toWriteMain, toWriteSide, 1);
                }else{
                    firstNumber = result;
                    result = 0d;
                    doneFirstNumber = true;
                    doneSecondNumber = false;
                    firstTimeSecondNumber = true;
                    digits = 0; 
                    hasComma=false;
                    String toWriteSide = Double.toString(firstNumber)+" * "; 
                    String toWriteMain = Double.toString(firstNumber);
                    overwriteNumber(toWriteMain, toWriteSide, 0);
                }
                break;
            case "-":
                operation = "-";
                if(doneFirstNumber == false){
                    String toWriteSide = Double.toString(firstNumber)+" - "; 
                    String toWriteMain = Double.toString(firstNumber);
                    doneFirstNumber = true;
                    firstTimeSecondNumber = true;
                    digits = 0; 
                    hasComma=false;
                    overwriteNumber(toWriteMain, toWriteSide, 0);
                }else if(doneSecondNumber == false){
                    String toWriteSide = Double.toString(secondNumber);
                    result = firstNumber - secondNumber;
                    String toWriteMain = Double.toString(result);
                    secondNumber = 0d;
                    firstNumber = 0d;
                    doneSecondNumber = true;
                    digits = 0; 
                    hasComma=false;
                    overwriteNumber(toWriteMain, toWriteSide, 1);
                }else{
                    firstNumber = result;
                    result = 0d;
                    doneFirstNumber = true;
                    doneSecondNumber = false;
                    firstTimeSecondNumber = true;
                    digits = 0; 
                    hasComma=false;
                    String toWriteSide = Double.toString(firstNumber)+" - "; 
                    String toWriteMain = Double.toString(firstNumber);
                    overwriteNumber(toWriteMain, toWriteSide, 0);
                }
                break;
            case "+":
                if(doneFirstNumber == false){
                    String toWriteSide = Double.toString(firstNumber)+" + "; 
                    String toWriteMain = Double.toString(firstNumber);
                    doneFirstNumber = true;
                    firstTimeSecondNumber = true;
                    digits = 0; 
                    hasComma=false;
                    operation = "+";
                    overwriteNumber(toWriteMain, toWriteSide, 0);
                }else if(doneSecondNumber == false){
                    String toWriteSide = Double.toString(secondNumber);
                    result = firstNumber + secondNumber;
                    String toWriteMain = Double.toString(result);
                    secondNumber = 0d;
                    firstNumber = 0d;
                    doneSecondNumber = true;
                    digits = 0; 
                    operation = "+";
                    hasComma=false;
                    overwriteNumber(toWriteMain, toWriteSide, 1);
                }else{
                    firstNumber = result;
                    secondNumber = 0d;
                    result = 0d;
                    doneFirstNumber = true;
                    doneSecondNumber = false;
                    firstTimeSecondNumber = true;
                    digits = 0; 
                    hasComma=false;
                    String toWriteSide = Double.toString(firstNumber)+" + "; 
                    String toWriteMain = Double.toString(firstNumber);
                    overwriteNumber(toWriteMain, toWriteSide, 0);
                    operation = "+";
                }
                break;
            case "=":
                if(doneFirstNumber == false){
                    String toWriteSide = Double.toString(firstNumber)+" + 0 = "; 
                    String toWriteMain = Double.toString(firstNumber);
                    doneFirstNumber = true;
                    doneSecondNumber = true;
                    digits = 0; 
                    hasComma=false;
                    overwriteNumber(toWriteMain, toWriteSide, 0);
                    
                }else if(doneSecondNumber == false){
                    if (secondNumber == 0d) secondNumber = firstNumber;
                    String toWriteSide = Double.toString(secondNumber) + "= ";
                    switch (operation){
                        case "+":
                            result = firstNumber + secondNumber;
                            break;
                        case "-":
                            result = firstNumber - secondNumber;
                            break;
                        case "x":
                            result = firstNumber * secondNumber;
                            break;
                        case "/":
                            result = firstNumber / secondNumber;
                            break;
                        case "^":
                            result = Math.pow(firstNumber, secondNumber);
                            break;
                    }
                    String toWriteMain = Double.toString(result);
                    firstNumber = 0d;
                    doneSecondNumber = true;
                    digits = 0; 
                    hasComma=false;
                    overwriteNumber(toWriteMain, toWriteSide, 1);
                }else{
                    String toWriteSide = Double.toString(result)+" = " + Double.toString(result); 
                    String toWriteMain = Double.toString(result);
                    overwriteNumber(toWriteMain, toWriteSide, 0);
                }
                break;
            case ",":
                if(hasComma == false){
                    if (doneFirstNumber == false){
                        if(firstNumber == 0d)   overwriteNumber("0,", "", 0);
                        overwriteNumber(",", "", 1);
                        hasComma = true;
                    }else if (doneSecondNumber == false){
                        if (secondNumber == 0d) overwriteNumber("0,", "", 1);
                        overwriteNumber(",", "", 1);
                        hasComma = true;
                    }else {
                        overwriteNumber("0,", "", 0);
                        hasComma = true;
                    }
                }
                break;
            case "+/-":
                if(doneFirstNumber == false){
                    firstNumber *= -1;
                    if (firstNumber > 0d)   changeNegativeSign(0);
                    else if(firstNumber < 0d)   changeNegativeSign(1);
                }else if (doneSecondNumber == false){
                    secondNumber *= -1;
                    if(secondNumber > 0d)   changeNegativeSign(0);
                    else if(secondNumber < 0d)  changeNegativeSign(1);
                }else {
                    doneFirstNumber = false;
                    doneSecondNumber = false;
                    firstTimeSecondNumber = false;
                    firstNumber = result;
                    firstNumber *= -1;
                    if (firstNumber > 0d)   changeNegativeSign(0);
                    else if(firstNumber < 0d)   changeNegativeSign(1);
                    hasComma = false;
                    digits = 0;
                }
                break;
            default:
                break;
        }
    }

    


}
