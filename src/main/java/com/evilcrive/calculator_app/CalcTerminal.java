package com.evilcrive.calculator_app;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Logger;

public class CalcTerminal {
    Integer num1;
    Integer num2;
    String s;
    Logger log = Logger.getLogger("main2");
    private CalcTerminal() throws IOException {
        BufferedReader reader = new BufferedReader( new InputStreamReader(System.in));
        log.info("Enter the first number:");
        s = reader.readLine();
        num1 = Integer.parseInt(s);
        log.info("Enter the second number:");
        s = reader.readLine();
        num2 = Integer.parseInt(s);

        log.info("Sum: "+(num1+num2) );
        log.info("Subtraction: "+(num1-num2));
        log.info("Multiplication: "+(num1*num2));
        log.info("Division: "+((float)num1/(float)num2));
    }
    public static void app() throws IOException {
        new CalcTerminal();
    }
}
