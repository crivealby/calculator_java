package com.evilcrive.calculator_app;

public class CalcController {
    CalcGUI view;
    CalcModel model;
    
    public void linkView(CalcGUI obj){
        view = obj;
    }

    public void linkModel(CalcModel obj){
        model = obj;
    }

    //input from view
    public void clickedButton(String event){
        System.out.println(event);
        model.clickedButton(event);
    }

    //responses from model
    public void resetText(Integer arg){
        view.resetText(arg);
    }
    public void writeDigitMain(Integer arg, Integer arg1){
        view.writeDigitMain(arg, arg1);
    }
    public void overwriteNumber(String toWriteMain, String toWriteSide, Integer arg){
        view.overwriteNumber(toWriteMain, toWriteSide, arg);
    }
    public void changeNegativeSign(Integer arg){
        view.changeNegativeSign(arg);
    }
    public void removeLastChar(){
        view.removeLastChar();
    }

}
