package com.evilcrive.calculator_app;


import javax.swing.*;

import java.util.ArrayList;

import java.awt.Color;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.event.ActionListener;

public class CalcGUI implements ActionListener{

    private CalcController controllerCalc;

    private JPanel mainPanel;
    private JFrame frame;
    GroupLayout layout;

    private ArrayList<JButton> buttons;  //watch resources or to see funct createButtonsTexts to see which button and their index on the arraylist
    private ArrayList<JTextField> texts; // 2 elements, result textfield, and the one with operation before
    

    private CalcGUI() {
        controllerCalc = new CalcController();
        
        mainPanel = new JPanel();
        frame = new JFrame();
        layout = new GroupLayout(mainPanel);


        mainPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1, true));
        mainPanel.setLayout(layout);

        createButtonsTexts();
        manageLayouts(layout, buttons, texts);
        
        frame.add(mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Crivez's Calculator");
        frame.setMinimumSize(new Dimension(400,400));
        frame.setVisible(true);
    }
    

    private void createButtonsTexts(){
        buttons = new ArrayList<JButton>(24);
        texts = new ArrayList<JTextField>(2);
        
        JTextField text0 = new JTextField();
        text0.setSize(100, 50);
        text0.setEditable(false);
        text0.setHorizontalAlignment(JTextField.RIGHT);
        JTextField text1 = new JTextField();
        text0.setFont(new Font("Tahoma",1,10));
        text1.setSize(100, 100);
        text1.setEditable(false);
        text1.setHorizontalAlignment(JTextField.RIGHT);
        text1.setFont(new Font("Tahoma",1,30));

        texts.add(text0);
        texts.add(text1);

        for (Integer i = 0; i < 24; i++){
            JButton tmp =new JButton();
            tmp.setSize(200, 200);
            tmp.setFont(new Font("Tahoma",1,22));
            tmp.addActionListener(this);
            buttons.add(tmp);
        }

        // setText of all the 24 buttons from button 0 to button 23
        for (Integer i = 0; i < 10; i++){
            buttons.get(i).setText(Integer.toString(i));
        }
        buttons.get(10).setText("+/-");
        buttons.get(11).setText(",");
        buttons.get(12).setText("=");
        buttons.get(13).setText("+");
        buttons.get(14).setText("−");
        buttons.get(15).setText("x");
        buttons.get(16).setText("/");
        buttons.get(17).setActionCommand("sqrt");
        buttons.get(17).setPressedIcon(new ImageIcon(new ImageIcon("src/main/resources/sqrt.jpg").getImage().getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH)));
        buttons.get(17).setIcon(new ImageIcon(new ImageIcon("src/main/resources/sqrt.jpg").getImage().getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH)));
        buttons.get(17).setFocusable(false);
        buttons.get(18).setActionCommand("square");
        buttons.get(18).setIcon(new ImageIcon(new ImageIcon("src/main/resources/square.jpg").getImage().getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH)));
        buttons.get(18).setPressedIcon(new ImageIcon(new ImageIcon("src/main/resources/square.jpg").getImage().getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH)));
        buttons.get(18).setFocusable(false);
        buttons.get(19).setActionCommand("^");
        buttons.get(19).setIcon(new ImageIcon(new ImageIcon("src/main/resources/^.jpg").getImage().getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH)));
        buttons.get(19).setPressedIcon(new ImageIcon(new ImageIcon("src/main/resources/^.jpg").getImage().getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH)));
        buttons.get(19).setFocusable(false);
        buttons.get(20).setActionCommand("del");
        buttons.get(20).setIcon(new ImageIcon(new ImageIcon("src/main/resources/del.jpg").getImage().getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH)));
        buttons.get(20).setPressedIcon(new ImageIcon(new ImageIcon("src/main/resources/del.jpg").getImage().getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH)));
        buttons.get(20).setFocusable(false);
        buttons.get(21).setText("C");
        buttons.get(22).setText("CE");
        buttons.get(23).setActionCommand("10^x");
        buttons.get(23).setIcon(new ImageIcon(new ImageIcon("src/main/resources/10^x.jpg").getImage().getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH)));
        buttons.get(23).setPressedIcon(new ImageIcon(new ImageIcon("src/main/resources/10^x.jpg").getImage().getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH)));
        buttons.get(23).setFocusable(false);

    }


    private void manageLayouts(GroupLayout layout, ArrayList<JButton> buttonsList, ArrayList<JTextField> textsList){
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
            .addGap(0, 1, 5)
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(textsList.get(0), GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                .addComponent(textsList.get(1), 0, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE))
            .addGap(0, 1, 5)
            )
        .addGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(buttonsList.get(23), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(19), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(7), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(4), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(1), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(10), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(buttonsList.get(22), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(18), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(8), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(5), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(2), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(0), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(buttonsList.get(21), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(17), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(9), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(6), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(3), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(11), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(buttonsList.get(20), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(16), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(15), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(14), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(13), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(12), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        layout.setVerticalGroup(layout.createSequentialGroup()
            .addGap(0, 1, 15)
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                .addComponent(textsList.get(0), 30, GroupLayout.DEFAULT_SIZE, (Short.MAX_VALUE)/500))
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(textsList.get(1), 50, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGap(0, 1, 15)
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(buttonsList.get(23), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(22), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(21), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(20), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(buttonsList.get(19), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(18), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(17), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(16), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(buttonsList.get(7), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(8), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(9), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(15), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(buttonsList.get(4), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(5), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(6), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(14), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(buttonsList.get(1), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(2), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(3), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(13), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(buttonsList.get(10), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(0), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(11), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonsList.get(12), 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
   
    }


    private void linkController(CalcController obj){
        controllerCalc = obj;
    }


    private void colorCalculator(Integer color){
        if (color == 2){
            mainPanel.setBackground(new Color(167,170,207));
            texts.get(0).setBackground(new Color(172,190,221));
            texts.get(1).setBackground(new Color(112,146,190));
            for (int i = 0; i<10; i++)  buttons.get(i).setBackground(new Color(220,217,236));
            buttons.get(10).setBackground(new Color(238,207,239));
            buttons.get(11).setBackground(new Color(238,207,239));
            buttons.get(20).setBackground(new Color(238,207,239));
            for(int i = 11; i<24; i++)  if(i!=11 && i!=20)  buttons.get(i).setBackground(new Color(220, 217, 236)); 

        }else if (color == 3){
            mainPanel.setBackground(Color.WHITE);
            for(int i = 0; i < 2; i++)  texts.get(i).setBackground(new Color(253, 236, 255));
            buttons.get(23).setBackground(new Color(248, 105, 105));
            buttons.get(22).setBackground(new Color(248, 105, 105));
            buttons.get(19).setBackground(new Color(248, 105, 105));
            buttons.get(21).setBackground(new Color(250, 186, 90));
            buttons.get(18).setBackground(new Color(250, 186, 90));
            buttons.get(7).setBackground(new Color(250, 186, 90));
            buttons.get(4).setBackground(new Color(250, 243, 124));
            buttons.get(8).setBackground(new Color(250, 243, 124));
            buttons.get(17).setBackground(new Color(250, 243, 124));
            buttons.get(20).setBackground(new Color(250, 243, 124));
            buttons.get(1).setBackground(new Color(181, 230, 29));
            buttons.get(5).setBackground(new Color(181, 230, 29));
            buttons.get(9).setBackground(new Color(181, 230, 29));
            buttons.get(16).setBackground(new Color(181, 230, 29));
            buttons.get(15).setBackground(new Color(162, 222, 251));
            buttons.get(6).setBackground(new Color(162, 222, 251));
            buttons.get(2).setBackground(new Color(162, 222, 251));
            buttons.get(10).setBackground(new Color(162, 222, 251));
            buttons.get(0).setBackground(new Color(124, 171, 250));
            buttons.get(3).setBackground(new Color(124, 171, 250));
            buttons.get(14).setBackground(new Color(124, 171, 250));
            buttons.get(11).setBackground(new Color(201, 156, 250));
            buttons.get(12).setBackground(new Color(201, 156, 250));
            buttons.get(13).setBackground(new Color(201, 156, 250));
        }
    }
    

    public static void startCalcApp(Integer color){
        CalcController controller = new CalcController();
        CalcGUI view = new CalcGUI();
        CalcModel model = new CalcModel(controller);

        view.colorCalculator(color);
        view.linkController(controller);
        controller.linkView(view);
        controller.linkModel(model);
    }


    public static void app(final Integer color){
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                startCalcApp(color);
            }
        });
    }

    //**EVENTS:
    @Override
    public void actionPerformed(java.awt.event.ActionEvent e){
        controllerCalc.clickedButton(e.getActionCommand());
    }

    public void resetText(Integer args){
        if(args == 0){
            texts.get(1).setText("");
        }else if (args == 1){
            texts.get(1).setText("");
            texts.get(0).setText("");
        }else if (args == 2){
            texts.get(1).setText("");
            texts.get(0).setText("");
        }else if (args == 3){
            texts.get(1).setText("");
        }
    }
    public void writeDigitMain(Integer arg, Integer arg1){
        if(arg1 == 1) texts.get(1).setText(Integer.toString(arg));
        else    texts.get(1).setText(texts.get(1).getText()+Integer.toString(arg));
    }
    public void overwriteNumber(String toWriteMain, String toWriteSide, Integer arg){
        if (arg == 1)   toWriteSide = texts.get(0).getText() + toWriteSide;
        texts.get(1).setText(toWriteMain);
        texts.get(0).setText(toWriteSide);
    }
    public void changeNegativeSign(Integer arg){
        if (arg == 0){
            texts.get(1).setText("-"+texts.get(1).getText());
        }else if (arg == 1){
            texts.get(1).setText(texts.get(1).getText().substring(1));
        }
    }
    public void removeLastChar(){
        texts.get(1).setText(texts.get(1).getText().substring(0,texts.get(1).getText().length()-1));

    }

}
