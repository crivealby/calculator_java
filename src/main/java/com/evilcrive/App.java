package com.evilcrive;

import java.io.IOException;
import java.util.logging.Logger;

public class App {
    
    static Logger log = Logger.getLogger("main1");

    public static void main(String[] args) throws IOException {
        
        Integer selection = 1;
        if (args.length > 0 && Integer.parseInt(args[0]) > 1)    selection = 2;

        if (selection == 1) com.evilcrive.calculator_app.CalcTerminal.app();
        else com.evilcrive.calculator_app.CalcGUI.app(Integer.parseInt(args[0]));
    }
}
